<?php
 
class ReindexParentType extends eZWorkflowEventType
{
    const WORKFLOW_TYPE_STRING = "reindexparent";

	function ReindexParentType()
    {
        $this->eZWorkflowEventType( ReindexParentType::WORKFLOW_TYPE_STRING, ezpI18n::tr( 'kernel/workflow/event', "Reindex parent node" ) );
        $this->setTriggerTypes( array( 'content' => array( 'publish' => array( 'after' ) ) ) );
    }
 
    function execute( $process, $event )
    {
        $parameters = $process->attribute( 'parameter_list' );
        $objectID = $parameters['object_id'];

        $db = eZDB::instance();
 
        $pathString = $db->arrayQuery( "SELECT path_string
                                        FROM ezcontentobject_tree
                                        WHERE main_node_id=node_id
                                          AND contentobject_id=$objectID" );
        if ( $pathString )
        {
            $pathString = $pathString[0]['path_string'];
            $path = array_reverse( explode( '/', trim( $pathString, '/' ) ) );
            array_shift( $path );
            // $path now contains node IDs of all ancestors (starting with the parent node)
 
            foreach( $path as $ancestorNodeID )
            {
                /* Find object ID but only if the ancestor's object contains searchable
                   attribute of the childrenindexer datatype. */
                $ancestorObjectID = $db->arrayQuery( "SELECT a.contentobject_id
                                                      FROM ezcontentobject_tree t,
                                                           ezcontentobject_attribute a,
                                                           ezcontentclass_attribute ca
                                                      WHERE t.node_id=$ancestorNodeID
                                                        AND t.contentobject_id=a.contentobject_id
                                                        AND t.contentobject_version=a.version
                                                        AND a.contentclassattribute_id=ca.id
                                                        AND ca.version=0
                                                        AND ca.data_type_string='childrenindexer'
                                                        AND ca.is_searchable=1", array( 'limit' => 1 ) );
                if ( !$ancestorObjectID )
                {
                    break;
                }
 
                $ancestorObjectID = $ancestorObjectID[0]['contentobject_id'];
 
                eZContentOperationCollection::registerSearchObject( $ancestorObjectID, false );
            }
        }
 
        return eZWorkflowType::STATUS_ACCEPTED;
    }
}
 
eZWorkflowEventType::registerEventType( ReindexParentType::WORKFLOW_TYPE_STRING, "ReindexParentType" );

?>